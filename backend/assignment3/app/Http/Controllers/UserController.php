<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index() {
        $comments = Users::all();
        return response()->json($comments);
    }
    public function update(Request $request, $id)
    {
        Users::all()->find($id)->update($request->all());
    }

    public function find($id){
        return Users::all()->find($id);
    }

// --------------------- [ Register user ] ----------------------
    public function userPostRegistration(Request $request) {
        $request->validate([
            'username'=>'required',
            'password'=>'required',
            'name' => 'required'
        ]);
        $user = new \App\Users([
            'username' => $request->get('username'),
            'password' => $request->get('password'),
            'name' => $request->get('name')
        ]);
        $user->save();

        // if registration success then return with success message
        if(!is_null($user)) {
            return 'created';
        }

        // else return with error message
        else {
            return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }
    }

// --------------------- [ User login ] ---------------------
    public function userPostLogin(Request $request) {
        $username = $request->get('username');
        $password = $request->get('password');

        $data = User::all()->where('username', $username)->first();
        if($data) {
            if ($password == $data->password) {
                Session::put('id', $data->get('id'));
                Session::put ('name', $data->get('name'));
                Session::put ('username', $data->get('username'));
                Session::put ('login', TRUE);
                return $data->id;
            }
            else {
                return "not logged";
            }
        }
        else {
            return "not logged";
        }
    }

// ------------------- [ User logout function ] ----------------------
    public function logout(Request $request ) {
        Session::flush();
        return "you are logged out";
    }
    public function storeComment(Request $request){
        $request->validate([
            'comment'=>'required',
            'user_id'=>'required'
        ]);
        $user = Users::all()->find($request->get('user_id'));
        $comment = new \App\Comments([
            'comment' => $request->get('comment')
        ]);
        $comment->user()->associate($user);
        $comment->save();
        return 'comment saved';
    }
}
