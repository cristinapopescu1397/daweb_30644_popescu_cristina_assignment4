<?php

namespace App\Http\Controllers;
use App\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UsersController extends Controller{

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index() {
        $users = Users::all();
        return response()->json($users);
    }

    /**
     * Store a new user.
     *
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        $request->validate([
            'username'=>'required',
            'password'=>'required'
        ]);
        $user = new \App\Users([
            'username' => $request->get('username'),
            'password' => $request->get('password')
        ]);
        $user->save();
        return 'user saved';
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Model
     */
    public function edit($id)
    {
        return Users::all()->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        Users::all()->find($id)->update($request->all());
    }
}
