import React from "react";
import axios from "axios";
import CanvasJSReact from '../asserts/canvasjs.react';
const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dates = [];
class CommentsChart extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            comments: [],
            result: []
        };
    }
    componentDidMount() {
        this.getComments();
    }
    getComments(){
        axios.get('http://localhost:8000/api/comments')
            .then(response => {
                response.data.map(comment => {
                    let date = new Date(comment.created_at);
                    dates.push(date.toLocaleDateString().split("/")[1])
                });
            })
            .catch(error => {
                console.log(error)
            });
    }

    render() {
        function getOccurrence(value) {
            return dates.filter((v) => (parseInt(v) === value)).length;
        }
        const options = {
            animationEnabled: true,
            exportEnabled: true,
            theme: "light2", //"light1", "dark1", "dark2"
            title:{
                text: "Comments per day"
            },
            axisY: {
                title: "Number of Comments",
                includeZero: true
            },
            axisX: {
                title: "Day",
                interval: 3
            },
            data: [{
                type: "line", //change type to bar, line, area, pie, etc
                //indexLabel: "{y}", //Shows y value on all Data Points
                toolTipContent: "Day {x}: {y}",
                dataPoints: [
                    { x: 1, y: getOccurrence(1) },
                    { x: 2, y: getOccurrence(2) },
                    { x: 3, y: getOccurrence(3) },
                    { x: 4, y: getOccurrence(4) },
                    { x: 5, y: getOccurrence(5) },
                    { x: 6, y: getOccurrence(6) },
                    { x: 7, y: getOccurrence(7) },
                    { x: 8, y: getOccurrence(8) },
                    { x: 9, y: getOccurrence(9) },
                    { x: 10, y: getOccurrence(10) },
                    { x: 11, y: getOccurrence(11) },
                    { x: 12, y: getOccurrence(12) },
                    { x: 13, y: getOccurrence(13) },
                    { x: 14, y: getOccurrence(14) },
                    { x: 15, y: getOccurrence(15) },
                    { x: 16, y: getOccurrence(16) },
                    { x: 17, y: getOccurrence(17) },
                    { x: 18, y: getOccurrence(18) },
                    { x: 19, y: getOccurrence(19) },
                    { x: 20, y: getOccurrence(20) },
                    { x: 21, y: getOccurrence(21) },
                    { x: 22, y: getOccurrence(22) },
                    { x: 23, y: getOccurrence(23) },
                    { x: 24, y: getOccurrence(24) },
                    { x: 25, y: getOccurrence(25) },
                    { x: 26, y: getOccurrence(26) },
                    { x: 27, y: getOccurrence(27) },
                    { x: 28, y: getOccurrence(28) },
                    { x: 29, y: getOccurrence(29) },
                    { x: 30, y: getOccurrence(30) },
                    { x: 31, y: getOccurrence(31) }
                ]
            }]
        };
        return(
            <div>
                <CanvasJSChart options = {options}/>
            </div>
        );
    }
} export default CommentsChart