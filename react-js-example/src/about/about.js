import React from "react";
import {Button, Container, Jumbotron} from "react-bootstrap";
import film from "../images/film.mp4"
import {List, ListItem} from "react-mdl";
import FormGroup from "react-bootstrap/FormGroup";
import FormControl from "react-bootstrap/FormControl";
import { Card, CardText, CardTitle } from 'reactstrap';
import axios from "axios";
import CommentsChart from "./CommentsChart";
const divStyle = {
    display: 'flex',
    alignItems: 'left',
};
let comm;
class About extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            comments: [],
            users: []
        };
        this.addComment = this.addComment.bind(this);
    }
    componentDidMount() {
        this.getComments();
        this.getUsers();
    }
    getComments() {
            axios.get('http://localhost:8000/api/comments')
                .then(response => {
                    this.setState({
                        comments: response.data
                    })
                })
                .catch(error => {
                    console.log(error)
                });
    }
    getUsers() {
        axios.get('http://localhost:8000/api/users')
            .then(response => {
                this.setState({
                    users: response.data
                })
            });
    }
    addComment(){
        let comment = {
            comment: comm,
            user_id: localStorage.getItem('userId')
        };
        if(localStorage.getItem('isLoggedIn') === 'true') {
            axios.post('http://localhost:8000/api/comment-store', comment);
        } else if(localStorage.getItem('isLoggedIn') === 'false') {
            return(alert("login first"));
        }
    }
    render() {
        let content = {

            EN: {
                title: "Indoor Navigation System",
                subtitle: "What are indoor navigation project's goals?",
                list: {
                    value1: "High accuracy: The application should consistently guide users to their destinations within a reasonable distance.",
                    value2: "Intuitive user interface (UI): The application should have an easy-to-use\n" +
                        "                                        UI that displays navigation hints correctly based on the user’s current\n" +
                        "                                        state. The application should also take into account the obstacles surrounding the user to avoid displaying any incorrect hints. For instance,\n" +
                        "                                        it should not tell users to go straight if there is an obstacle immediately\n" +
                        "                                        ahead of them.",
                    value3: "No pre-loaded indoor maps: The application should be able to navigate\n" +
                        "                                        the user without requiring a pre-loaded map of the environment."
                }
            },

            RO: {
                title: "Sistem de Navigare Interioara",
                subtitle: "Care sunt obiectivele proiectului de nagivare interioara?",
                list: {
                    value1: "Precizie ridicată: aplicația ar trebui să ghideze constant utilizatorii către destinațiile lor la o distanță rezonabilă.",
                    value2: "Interfață pentru utilizator intuitivă (UI): Aplicația ar trebui să aibă o interfață de utilizator ușor de utilizat, care afișează corect indicii de navigare în funcție de starea actuală a utilizatorului. Aplicația ar trebui să țină cont și de obstacolele din jurul utilizatorului pentru a evita afișarea de indicii incorecte. De exemplu, nu ar trebui să le spună utilizatorilor să meargă direct dacă există un obstacol imediat înaintea lor.",
                    value3: "Fără hărți interioare încărcate în prealabil: aplicația trebuie să poată naviga utilizatorul fără a necesita o hartă preîncărcată a mediului."
                }
            }

        };
        localStorage.getItem("language") === "RO" ? (content = content.RO) : (content = content.EN);
        return(
            <div>
                <Jumbotron>
                    <h1>{content.title}</h1>
                    <br/>
                    <h4>
                        {content.subtitle}
                    </h4>
                    <Container style={divStyle}>
                        <p>
                            <video src={film} width="500" height="400" controls="controls" autoPlay={false} />
                        </p>
                            <List>
                                <br/> <br/>
                                <ListItem>
                                    <p> {content.list.value1} </p>
                                </ListItem>
                                <ListItem>
                                    <p>
                                        {content.list.value2}
                                    </p>
                                </ListItem>
                                <ListItem>
                                    <p>
                                        {content.list.value3}
                                    </p>
                                </ListItem>
                            </List>
                    </Container>
                    <hr/>
                    <CommentsChart/>
                    <hr/>
                    <FormGroup>
                        <FormControl
                            componentclass="text"
                            value={comm}
                            onChange={e => comm = e.target.value}
                            placeholder="Add a comment..."
                            style={{height: '50px'}}
                        />
                    </FormGroup>
                    <Button type="submit" onClick={this.addComment}>Submit</Button>
                    <hr/>
                    <Container fluid>
                            {this.state.comments.map(robot => {
                                return (
                                    <div>
                                        <Card>
                                            <div style={{textAlign:'left'}}>
                                                <CardTitle style={{fontWeight:'bold',fontSize:'1.3rem'}}>{this.state.users.map(user=> {
                                                    if(robot.user_id === user.id) {
                                                        return(user.name)
                                                    }
                                                })}</CardTitle>
                                                <CardText>{robot.comment}</CardText>
                                            </div>
                                        </Card>
                                        <br/>
                                    </div>
                                )
                            })
                            }
                    </Container>
                </Jumbotron>
            </div>
        );
    }
}

export default About;