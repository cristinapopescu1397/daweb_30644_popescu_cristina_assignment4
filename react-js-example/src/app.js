import React, {useState} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import News from './news/news'
import About from "./about/about";
import Profile from "./profile/profile";
import Coordinator from "./coordinator/coordinator";
import Contact from "./contact/contact";
import Login from "./login/login";
import background from "./images/background.png"
import ChangeDataModal from "./login/userProfile/userProfile";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1300px",
    backgroundImage: `url(${background})`
};

    function App() {
        let languageStoredInLocalStorage = localStorage.getItem("language");
        let [language, setLanguage] = useState(
            languageStoredInLocalStorage ? languageStoredInLocalStorage : "EN"
        );
        return (
            <div style={backgroundStyle}>
            <Router>
                <div className="container nav-fill">
                    <select
                        className="select"
                        value={language}
                        onChange={e => {
                            setLanguage(e.target.value);
                            storeLanguageInLocalStorage(e.target.value);
                        }}
                    >
                        <option value="EN">EN</option>
                        <option value="RO">RO</option>
                    </select>
                    <NavigationBar />
                    <Switch>
                        <Route
                            exact
                            path='/'
                            render={() => <Home />}
                        />
                        <Route
                            exact
                            path='/news'
                            render={() => <News/>}
                        />
                        <Route
                            exact
                            path='/about'
                            render={() => <About/>}
                        />
                        <Route
                            exact
                            path='/profile'
                            render={() => <Profile/>}
                        />
                        <Route
                            exact
                            path='/coordinator'
                            render={() => <Coordinator/>}
                        />
                        <Route
                            exact
                            path='/contact'
                            render={() => <Contact/>}
                        />
                        <Route
                            exact
                            path='/login'
                            render={() => <Login/>}
                        />
                        <Route
                            exact
                            path='/changeData'
                            render={() => <ChangeDataModal/>}
                        />
                    </Switch>
                </div>
            </Router>
            </div>
        );
}
function storeLanguageInLocalStorage(language) {
    localStorage.setItem("language", language);
}
export default App
