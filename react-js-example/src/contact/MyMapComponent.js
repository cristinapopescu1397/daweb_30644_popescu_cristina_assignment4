/*global google*/
import React from 'react'
import  { compose, withProps, lifecycle } from 'recompose'
import {withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer} from 'react-google-maps'
let currentLatLng = {
    lat: 0,
    lng: 0
};
class MyMapComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentLatLng: {
                lat: 0,
                lng: 0
            },
            isMarkerShown: false
        }
    }
    showCurrentLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                position => {
                            currentLatLng.lat = position.coords.latitude;
                            currentLatLng.lng = position.coords.longitude;
                    })
                }
    };
    componentDidMount() {
        this.showCurrentLocation()
    }

    render() {
        const DirectionsComponent = compose(
            withProps({
                googleMapURL: `https://maps.googleapis.com/maps/api/js?key=AIzaSyAtvRHJoq4-u7E0LcknmDpSprvsxlFTtHs&&v=3.exp&libraries=geometry,drawing,places`,
                loadingElement: <div style={{ height: `400px` }} />,
                containerElement: <div style={{ width: `100%` }} />,
                mapElement: <div style={{height: `600px`, width: `600px` }}  />,
            }),
            withScriptjs,
            withGoogleMap,
            lifecycle({
                componentDidMount() {
                    const DirectionsService = new google.maps.DirectionsService();
                    DirectionsService.route({
                        origin: new google.maps.LatLng(currentLatLng.lat, currentLatLng.lng),
                        destination: new google.maps.LatLng(46.772443, 23.585240),
                        travelMode: google.maps.TravelMode.DRIVING,
                    }, (result, status) => {
                        if (status === google.maps.DirectionsStatus.OK) {
                            this.setState({
                                directions: {...result},
                                markers: true
                            })
                        } else {
                            console.error(`error fetching directions ${result}`);
                        }
                    });
                }
            })
        )(props =>
            <GoogleMap
                defaultZoom={3}
            >
                {props.directions && <DirectionsRenderer directions={props.directions} suppressMarkers={props.markers}/>}
            </GoogleMap>
        );
        return (
            <DirectionsComponent
            />
        )
    }
}
export default MyMapComponent