import React from "react";
import contact from '../images/contactlogo.png'
import {SocialIcon} from 'react-social-icons';
import {Jumbotron} from "react-bootstrap";
import MyMapComponent from "./MyMapComponent";


class Contact extends React.Component{
    render() {
        let content = {

            EN: {
                university: "Technical University of Cluj-Napoca",
                phone: "Phone: 0744195713",
            },

            RO: {
                university: "Universitatea Tehnică din Cluj-Napoca",
                phone: "Telefon: 0744195713",
            }

        };
        localStorage.getItem("language") === "RO" ? (content = content.RO) : (content = content.EN);
        return(
            <Jumbotron>
            <section className="container-1">

                <img id="contact" src={contact} width="180" height="180" alt="contactlogo"/>
                <h3> <strong>Cristina Popescu</strong></h3>
                <h4>{content.university}</h4>
                <h5>{content.phone}</h5>

                <SocialIcon url="https://www.facebook.com/cristina.popescu.587"/>
                <SocialIcon url="https://www.linkedin.com/in/cristina-popescu-7863aa150/"/>
                <hr/>
            </section>
                <MyMapComponent/>
            </Jumbotron>
        );
    }
}

export default Contact;