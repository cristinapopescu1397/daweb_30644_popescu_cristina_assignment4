import React from "react";
import {List, ListItem, ListItemContent} from 'react-mdl'
import {Container, Jumbotron} from "react-bootstrap";
import madalin from '../images/madalin.png'
const divStyle = {
    display: 'flex',
    alignItems: 'center'
};
class Coordinator extends React.Component{
    render() {
        let content = {

            EN: {
                title: "Teaching",
                list: {
                    value1: "Programming in Assembly Language",
                    value2: "Microprocessor-based Systems",
                    value3: "Structure of Computer Systems",
                    value4: "Industrial Informatics"
                }
            },

            RO: {
                title: "Materii predate",
                list: {
                    value1: "Programare in Limbaj de Asamblare",
                    value2: "Sisteme Bazate pe Microprocesor",
                    value3: "Structura Sistemelor de Calcul",
                    value4: "Informatica Industriala"
                }
                }

        };
        localStorage.getItem("language") === "RO" ? (content = content.RO) : (content = content.EN);
        return(
            <div>
                <Jumbotron fluid style={divStyle}>
                    <Container>
                        <img
                            className="avatar-img"
                            src={madalin}
                            alt="avatar"
                        />
                        <h2 align="center">Madalin Neagu</h2>
                    </Container>
                    <br/>
                    <Container>
                        <h3>{content.title}</h3>
                        <hr/>

                        <div className="contact-list">
                            <List>
                                <ListItem>
                                    <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                                        {content.list.value1}
                                    </ListItemContent>
                                </ListItem>

                                <ListItem>
                                    <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                                        {content.list.value2}
                                    </ListItemContent>
                                </ListItem>

                                <ListItem>
                                    <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                                        {content.list.value3}
                                    </ListItemContent>
                                </ListItem>

                                <ListItem>
                                    <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                                        {content.list.value4}
                                    </ListItemContent>
                                </ListItem>


                            </List>
                        </div>
                    </Container>
                </Jumbotron>
            </div>
        );
    }
}

export default Coordinator;