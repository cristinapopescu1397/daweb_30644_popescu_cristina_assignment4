import React, {useState} from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import FormGroup from 'react-bootstrap/FormGroup';
import FormControl from 'react-bootstrap/FormControl';
import {Button, Jumbotron} from 'react-bootstrap';
import axios from 'axios';
export default function Login() {
    const [key, setKey] = useState('login');

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');

    function handleLoginSubmit(e) {
        let loginData = {
            username: email,
            password: password
        };
         console.log(loginData);
        axios.post('http://localhost:8000/api/login', loginData)
            .then(response => {
                localStorage.setItem('userId', response.data);
                localStorage.setItem('isLoggedIn', 'true');
            })
            .catch(error => {
                console.log(error);
            });
        e.preventDefault();
    }

    function handleRegisterSubmit(e) {
        let data = {
            username: email,
            password: password,
            name: name
        };

        axios.post('http://localhost:8000/api/user-store', data)
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error)
            });
        console.log(data);
        e.preventDefault();
    }

    function validateForm() {
        return email.length > 0 && password.length > 0;
    }

    return (
        <div>
            <Jumbotron>
                    <Tabs
                        id="controlled-tab-example"
                        activeKey={key}
                        onSelect={(k) => setKey(k)}
                    >
                        <Tab eventKey="login" title="Login">
                            <form onSubmit={handleLoginSubmit}>
                                <FormGroup controlId="email" bssize="large">
                                    <label>Email</label>
                                    <FormControl
                                        autoFocus
                                        type="email"
                                        value={email}
                                        onChange={e => setEmail(e.target.value)}
                                    />
                                </FormGroup>
                                <FormGroup controlId="password" bssize="large">
                                    <label>Password</label>
                                    <FormControl
                                        type="password"
                                        value={password}
                                        onChange={e => setPassword(e.target.value)}
                                    />
                                </FormGroup>
                                <Button block bssize="large" disabled={!validateForm()} type="submit">
                                    Login
                                </Button>
                            </form>
                        </Tab>
                        <Tab eventKey="register" title="Register">
                            <form onSubmit={handleRegisterSubmit}>
                                <FormGroup controlId="name" bssize="large">
                                    <label>Name</label>
                                    <FormControl
                                        autoFocus
                                        value={name}
                                        onChange={e => setName(e.target.value)}
                                    />
                                </FormGroup>
                                <FormGroup controlId="emailRegister" bssize="large">
                                    <label>Email</label>
                                    <FormControl
                                        autoFocus
                                        type="email"
                                        value={email}
                                        onChange={e => setEmail(e.target.value)}
                                    />
                                </FormGroup>
                                <FormGroup controlId="passwordRegister" bssize="large">
                                    <label>Password</label>
                                    <FormControl
                                        type="password"
                                        value={password}
                                        onChange={e => setPassword(e.target.value)}
                                    />
                                </FormGroup>
                                <Button block bssize="large" disabled={!validateForm()} type="submit">
                                    Register
                                </Button>
                            </form>
                        </Tab>
                    </Tabs>
            </Jumbotron>
        </div>
    );
}