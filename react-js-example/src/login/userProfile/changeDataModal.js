import React, {useState} from 'react'
import FormGroup from "react-bootstrap/FormGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import {FormLabel, Jumbotron} from "react-bootstrap";
import axios from 'axios'
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import FormCheck from "react-bootstrap/FormCheck";
let interests1 = [];
export default function ChangeDataModal(){
    const[name, setName] = useState('');
    const[email, setEmail] = useState('');
    const[password, setPassword] = useState('');
    const[photo, setPhoto] = useState('');
    function fileSelectedHandler(event) {
        setPhoto(URL.createObjectURL(event.target.files[0]));
    }
    function updateData(event) {
        interests1 = interests1.toString();
        let data = {
            username: email,
            password: password,
            name: name,
            photo: photo,
            interests: interests1
        };
        axios.put('http://localhost:8000/api/update/' + localStorage.getItem('userId'), data);
        event.preventDefault();
    }
        return (
            <>
                <Jumbotron>
                    <form>
                        <FormGroup controlId="email" bssize="large">
                            <label>Name</label>
                            <FormControl
                                autoFocus
                                value={name}
                                onChange={e => setName(e.target.value)}
                            />
                            <label>Email</label>
                            <FormControl
                                autoFocus
                                type="email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup controlId="password" bssize="large">
                            <label>Password</label>
                            <FormControl
                                type="password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                            />
                        </FormGroup>
                        <fieldset>
                            <FormGroup as={Row}>
                                <FormLabel as="legend" column sm={1}>
                                    Interests:
                                </FormLabel>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                <Col sm={3}>
                                    <FormCheck
                                        type="radio"
                                        label="Interest1"
                                        name="Interest1"
                                        id="formHorizontalRadios1"
                                        onChange={e => interests1.push(e.target.name)}
                                    />
                                    <FormCheck
                                        type="radio"
                                        label="Interest2"
                                        name="Interest2"
                                        id="formHorizontalRadios2"
                                        onChange={e => interests1.push(e.target.name)}
                                    />
                                    <FormCheck
                                        type="radio"
                                        label="Interest3"
                                        name="Interest3"
                                        id="formHorizontalRadios3"
                                        onChange={e => interests1.push(e.target.name)}
                                    />
                                    <FormCheck
                                        type="radio"
                                        label="Interest4"
                                        name="Interest4"
                                        id="formHorizontalRadios4"
                                        onChange={e => interests1.push(e.target.name)}
                                    />
                                </Col>
                            </FormGroup>
                        </fieldset>
                        <input type="file" onChange={fileSelectedHandler}/>
                        <img src={photo} alt="avatar"/>
                        <br />
                        <Button block bssize="large" type="submit" onClick={updateData}>
                            Change
                        </Button>
                    </form>
                </Jumbotron>
            </>
        );
}