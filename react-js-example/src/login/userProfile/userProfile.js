import React from "react";
import {Button, Container, FormLabel, Jumbotron} from "react-bootstrap";
import axios from "axios";
import ShowModal from "./showModal";
import FormGroup from "react-bootstrap/FormGroup";
const divStyle = {
    display: 'flex',
    alignItems: 'center'
};

class UserProfile extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            username: null,
            name: null,
            password: null,
            interests: null,
            photo: null,
            showModal: false
        };
        this.showEditModal=this.showEditModal.bind(this)
    }
    showEditModal() {
        let visible = true;
        this.setState({showModal: visible});
    }
    componentDidMount() {
        this.getUser();
    }
    getUser() {
        console.log(localStorage.getItem('userId'));
        axios.get('http://localhost:8000/api/find/' + localStorage.getItem('userId'))
            .then(response => {
                console.log(response);
                this.setState({
                    username: response.data.username,
                    name: response.data.name,
                    password: response.data.password,
                    photo: response.data.photo,
                    interests: response.data.interests
                })
            })
            .catch(error => {
                console.log(error)
            });
    }
    render() {
        if(localStorage.getItem('isLoggedIn') === 'true') {
        return(
            <div>
                <Jumbotron fluid style={divStyle}>
                    <Container>
                        <img
                            className="avatar-img"
                            src={this.state.photo}
                            alt="upload"/>
                        <h2 align="center">{this.state.name}</h2>
                    </Container>
                    <br/>
                    <Container>
                        <FormGroup>
                            <FormLabel>E-mail: </FormLabel>
                            &nbsp;
                            <text>{this.state.username}</text>
                        </FormGroup>
                        <FormGroup>
                            <FormLabel>Interests: </FormLabel>
                            &nbsp;
                            <text>{this.state.interests}</text>
                        </FormGroup>
                        <hr/>
                        <Button onClick={this.showEditModal}>Edit Profile</Button>
                    </Container>
                </Jumbotron>
                <ShowModal show={this.state.showModal} onHide={() => {this.setState({showModal: false}); window.location=window.location.href;}}/>
            </div>
        );
        } else {
            return(<Jumbotron><h4>login first</h4></Jumbotron>);
        }
    }
}

export default UserProfile;