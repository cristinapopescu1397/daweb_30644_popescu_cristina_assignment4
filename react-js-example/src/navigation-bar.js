import React from 'react'
import './navigation-bar.css'
import {
    Nav,
    Navbar,
    NavLink
} from 'reactstrap';
import logo from './images/FAVPNG_location-icon-misc-icon-pin-icon_FQ6bbF2n.png'
import NavbarBrand from "react-bootstrap/NavbarBrand";
import Dropdown from "react-bootstrap/Dropdown";
const dropdownStyle = {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    color: ''
};
class NavigationBar extends React.Component {
    state = {
            login: localStorage.getItem('isLoggedIn')
    };
    logout = eventKey => {
        if(eventKey === 'logout') {
            console.log(this.state.login);
            localStorage.setItem('isLoggedIn', 'false');
            this.setState({login: 'false'});
            console.log(localStorage.getItem('isLoggedIn'))
        }
    };
    render(){
        let dropdown;
        if (this.state.login === 'true') {
            dropdown = <Dropdown.Menu>
                <Dropdown.Item href="/changeData">Profile settings</Dropdown.Item>
                <Dropdown.Item href="/" eventKey='logout' onSelect={e => this.logout(e)}>
                    Logout
                </Dropdown.Item>
            </Dropdown.Menu>
        } else if(this.state.login === 'false') {
            dropdown = <Dropdown.Menu>
                <Dropdown.Item href="/login">Login</Dropdown.Item>
            </Dropdown.Menu>
        }
        let content = {

            EN: {
                home: "Home",
                news: "News",
                about: "About",
                profile: "Profile",
                coordinator: "Coordinator",
            },

            RO: {
                home: "Acasă",
                news: "Noutăți",
                about: "Despre",
                profile: "Profil",
                coordinator: "Coordonator",
            }

        };
        localStorage.getItem("language") === "RO" ? (content = content.RO) : (content = content.EN);
        return (
            <div>
                <Navbar className="menu" expand="md">
                    <NavbarBrand href="/">
                        <img src={logo} width={"40"} alt="logo"/>
                    </NavbarBrand>
                    <Nav navbar>
                        <NavLink className="menu__link" href="/">
                            {content.home}
                        </NavLink>
                        <NavLink className="menu__link" href="/news">
                            {content.news}
                        </NavLink>
                        <NavLink className="menu__link" href="/about">
                            {content.about}
                        </NavLink>
                        <NavLink className="menu__link" href="/profile">
                            {content.profile}
                        </NavLink>
                        <NavLink className="menu__link" href="/coordinator">
                            {content.coordinator}
                        </NavLink>
                        <NavLink className="menu__link" href="/contact">
                            Contact
                        </NavLink>
                        <Dropdown style={dropdownStyle}>
                            <Dropdown.Toggle style={dropdownStyle}>
                                User
                            </Dropdown.Toggle>
                            {dropdown}
                        </Dropdown>
                    </Nav>
                </Navbar>
            </div>
        );
    }
}
export default NavigationBar