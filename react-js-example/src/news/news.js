import React from "react";
import {parseString} from 'xml2js'
const div = {
    backgroundColor: "white"
};
let en = null;
let content = null;
let res = null;
class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {dataSource: []}
    }

    componentDidMount() {
        const url = "daw2.xml";
        fetch(url)
            .then((response) => response.text())
            .then((responseText) => {
                parseString(responseText, function (err, result) {
                    res = result['news']['element'];
                });
            })
            .catch((error) => {
                console.log('Error fetching the feed: ', error);
            });
        fetch("daw2en.xml")
            .then((response) => response.text())
            .then((responseText) => {
                parseString(responseText, function (err, result) {
                    en = result['news']['element'];
                });
                this.setState({dataSource: en})
            })
            .catch((error) => {
                console.log('Error fetching the feed: ', error);
            });
    }
    render() {
        localStorage.getItem("language") === "RO" ? (content = res) : (content = this.state.dataSource);
        return (
            <div style={div}>
                {content}
            </div>
            );
    }
}
export default News;